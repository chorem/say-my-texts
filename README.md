# Say My Texts

Say My Texts is an Android application which reads out loud the SMS you receive.
This is particularly useful when you are riding, running or driving.
You also can reply to the sender by calling him or by dictating a new SMS.

The application is composed of:

- a service which reads the SMS out loud and manage the vocal commands
- an activity for the settings
- a *BroadcastReceiver* which starts the service when the phone finishes booting
- a *BroadcastReceiver* which listens to the bluetooth connections and the plug of a headset
- a *BroadcastReceiver* which receives the SMS
- a *BroadcastReceiver* which receives the vocal commands
- a *BroadcastReceiver* which receives the dictated reply
- an about dialog which shows info about the app
- an application to setup the crash report