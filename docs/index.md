![user documentation](img/smt_logo_title.png "Say My Texts user documentation")

Say My Texts is an Android application which reads out loud the SMS you receive.
This is particularly useful when you are riding, running or driving.
You also can reply to the sender by calling him or by dictating the reply.


## Installation

To install the application, you need a device with at least Android Ice Cream Sandwich 4.0.3.

The application needs a voice synthetizer. If none is installed, you will be able to install one from the application.

Once the application is installed, you need to launch it once to activate it.
It will be automatically activated the next times you start the device.


### Permissions

The application needs the following permissions:

- *RECEIVE_SMS* to receive the SMS
- *SEND_SMS* the user can send SMS to himself to test
- *READ_PHONE_STATE* to check that the user is not in a call
- *MODIFY_AUDIO_SETTINGS* the voice synthetizer needs it
- *READ_CONTACTS* to match the phone number of the sender with the contact name
- *BLUETOOTH* to get the connected devices
- *RECORD_AUDIO* pour les commandes vocales et dicter la réponse
- *CALL_PHONE* pour rappeler l'expéditeur


## Settings

When you launch the application from the menu, the settings open.
You can:

- enable or disable the reading depending if a headset is plugged or not or if a bluetooth device is connected
- enable or disable the interaction with the received messages (only if a voice recognizer is installed)
- if the interaction is enabled, you can configure the maximum number of tries to understand the vocal commands (3 by default)
- enable or disable the [Heisendroid mode](#Heisendroid_mode).
- send a SMS to yourself to test the reading

### Heisendroid mode

The name *Say My Texts* is a reference to the American TV show *Breaking Bad*.
The logo character's name is *Heisendroid*,
in reference to Walter White's pseudonym in the series (Heisenberg).

When you enable the Heisendroid mode, the SMS reading is preceded by "*Say my text*"
and followed by "*You're goddamn right!*". It is also a reference to one of the cult line of the series.


### Interactions

If the interaction is on, when you receive an SMS, the app offers you 3 choices:

- call the sender (say "call")
- reply by SMS by dictating the message (say "repy")
- do nothing (say "nothing")

If you choose to reply by SMS, you can then dictate the message. The application will tell what it understands.
You can then:

- confirm and send the reply (say "confirm")
- dictate the text again (say "modify")
- cancel the reply and quit  (say "cancel")

If the app does not understand what you said (it happens), you will be asked to repeat.

