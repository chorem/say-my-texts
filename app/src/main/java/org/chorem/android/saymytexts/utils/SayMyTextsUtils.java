/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class SayMyTextsUtils {

    public static boolean checkVoiceRecognition(Context context) {
        // Check if voice recognition is present
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        return activities.size() > 0;
    }

    public static final double QUAVER_DURATION = 0.25;
    public static final double BLACK_DURATION = 0.5;
    public static final double POINTED_BLACK_DURATION = 0.75;

    public static final int SAMPLE_RATE = 8000;

    public static final double G_FREQUENCY = 396; // hz
    public static final double A_FREQUENCY = 440; // hz
    public static final double B_FREQUENCY = 495; // hz
    public static final double C_FREQUENCY = 528; // hz
    public static final double D_FREQUENCY = 594; // hz

    private static final List<Byte> FS_SONG_NOTES = new ArrayList<>();

    public static void playFSSong() {
        if (FS_SONG_NOTES.isEmpty()) {
            initFSSong();
        }
        playSound();
    }

    private static void initFSSong() {
        List<Double> tones = new ArrayList<>();

        genTone(tones, D_FREQUENCY, BLACK_DURATION);
        genTone(tones, C_FREQUENCY, QUAVER_DURATION);
        genTone(tones, B_FREQUENCY, BLACK_DURATION);
        genTone(tones, A_FREQUENCY, BLACK_DURATION);
        genTone(tones, B_FREQUENCY, BLACK_DURATION);
        genTone(tones, C_FREQUENCY, QUAVER_DURATION);
        genTone(tones, B_FREQUENCY, BLACK_DURATION);
        genTone(tones, A_FREQUENCY, BLACK_DURATION);
        genTone(tones, G_FREQUENCY, BLACK_DURATION);
        genTone(tones, G_FREQUENCY, POINTED_BLACK_DURATION);
        genTone(tones, A_FREQUENCY, POINTED_BLACK_DURATION);
        genTone(tones, B_FREQUENCY, QUAVER_DURATION);
        genTone(tones, C_FREQUENCY, POINTED_BLACK_DURATION);
        genTone(tones, B_FREQUENCY, BLACK_DURATION);
        genTone(tones, B_FREQUENCY, QUAVER_DURATION);
        genTone(tones, D_FREQUENCY, BLACK_DURATION);

        finalizeSample(tones);
    }

    private static void genTone(List<Double> tones, double freqOfTone, double duration){
        // fill out the array
        int numSample = (int) (duration * SAMPLE_RATE);
        for (int i = 0; i < numSample; ++i) {
            tones.add(Math.sin(2 * Math.PI * (tones.size()) / (SAMPLE_RATE / freqOfTone)));
        }

    }

    private static void finalizeSample(List<Double> tones) {
        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : tones) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            FS_SONG_NOTES.add((byte) (val & 0x00ff));
            FS_SONG_NOTES.add((byte) ((val & 0xff00) >>> 8));

        }
    }

    private static void playSound(){
        int size = FS_SONG_NOTES.size();
        final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                                                     SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO,
                                                     AudioFormat.ENCODING_PCM_16BIT, size,
                                                     AudioTrack.MODE_STATIC);

        byte[] bytes = new byte[size];
        for (int i = 0 ; i < size ; i++) {
            bytes[i] = FS_SONG_NOTES.get(i);
        }
        audioTrack.write(bytes, 0, size);
        audioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
            @Override
            public void onMarkerReached(AudioTrack track) {
            }

            @Override
            public void onPeriodicNotification(AudioTrack track) {
                if (track.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
                    audioTrack.release();
                }
            }
        });
        audioTrack.play();
    }

    /**
     * Finds the contact name in the contact book
     * @param number the number of the contact
     * @return the name if the contact is known, the number otherwise
     */
    public static String getContactDisplayNameByNumber(Context context, String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String name = number;

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri,
                                                     new String[] { BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME },
                                                     null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return name;
    }
}
