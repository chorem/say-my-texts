/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.broadcastreceiver;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.telephony.SmsManager;
import android.util.Log;
import org.chorem.android.saymytexts.R;
import org.chorem.android.saymytexts.SayMyTextService;
import org.chorem.android.saymytexts.SayMyTextsApplication;
import org.chorem.android.saymytexts.model.SMS;
import org.chorem.android.saymytexts.utils.SayMyTextsRecognitionListener;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class SayNextActionBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "SayNextActionBroadcastReceiver";

    public static final String ACTION_SAY_NEXT_ACTION = "org.chorem.android.saymytexts.SAY_NEXT_ACTION";

    public static final String INTENT_EXTRA_SMS = "sms";
    public static final String INTENT_EXTRA_MESSAGE = "message";
    public static final String INTENT_EXTRA_ATTEMPT_NUMBER = "attemptNumber";
    public static final String INTENT_EXTRA_FALLBACK_ACTION = "fallbackAction";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "next action ?");
        }
        final SMS sms = (SMS) intent.getSerializableExtra(INTENT_EXTRA_SMS);

        if (sms != null) {
            final SpeechRecognizer speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
            speechRecognizer.setRecognitionListener(new SayMyTextsRecognitionListener(context, intent, sms) {

                @Override
                protected void onError(Context context, Intent intent, SMS sms, int error) {
                    reaskAction(context, intent, sms);
                }

                @Override
                public void onResults(Bundle data) {
                    List<String> results = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.d(TAG, "results " + results);
                    }
                    if (results != null) {

                        if (results.contains(context.getString(R.string.call_action))) {
                            try {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                callIntent.setData(Uri.parse("tel:" + sms.getSenderNumber()));
                                context.startActivity(callIntent);

                            } catch (ActivityNotFoundException activityException) {
                                if (SayMyTextsApplication.LOG_ENABLED) {
                                    Log.e(TAG, "Calling a Phone Number failed", activityException);
                                }
                                tg.startTone(ToneGenerator.TONE_PROP_NACK);
                                readNext(context);
                            }

                        } else if (results.contains(context.getString(R.string.answer_action))
                                || results.contains(context.getString(R.string.modifiy_action))) {

                            if (SayMyTextsApplication.LOG_ENABLED) {
                                Log.d(TAG, "Répondre ou corriger");
                            }
                            Intent serviceIntent = new Intent(context, SayMyTextService.class);
                            serviceIntent.setAction(SayMyTextService.ACTION_DICTATE_SMS);
                            serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_SMS, sms);
                            context.startService(serviceIntent);

                        } else if (results.contains(context.getString(R.string.confirm_action))) {
                            String message = intent.getStringExtra(INTENT_EXTRA_MESSAGE);
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(sms.getSenderNumber(), null, message, null, null);

                            tg.startTone(ToneGenerator.TONE_PROP_ACK);

                            readNext(context);

                        } else if (results.contains(context.getString(R.string.quit_action))
                                || results.contains(context.getString(R.string.cancel_action))) {
                            // do nothing
                            if (SayMyTextsApplication.LOG_ENABLED) {
                                Log.d(TAG, "Quitter");
                            }
                            tg.startTone(ToneGenerator.TONE_PROP_ACK);

                            readNext(context);

                        } else {
                            tg.startTone(ToneGenerator.TONE_PROP_NACK);
                            reaskAction(context, intent, sms);
                        }

                    } else {
                        tg.startTone(ToneGenerator.TONE_PROP_NACK);
                        reaskAction(context, intent, sms);
                    }
                }
            });

            Intent recognizeIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            // Specify free form input
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                                     RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

            speechRecognizer.startListening(recognizeIntent);
        }
    }

    protected void reaskAction(Context context, Intent intent, SMS sms) {
        int attemptNumber = intent.getIntExtra(INTENT_EXTRA_ATTEMPT_NUMBER, 1);
        String fallbackAction = intent.getStringExtra(INTENT_EXTRA_FALLBACK_ACTION);
        String dictatedMessage = intent.getStringExtra(INTENT_EXTRA_MESSAGE);

        Intent serviceIntent = new Intent(context, SayMyTextService.class);
        serviceIntent.setAction(fallbackAction);
        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_SMS, sms);
        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_ATTEMPT_NUMBER, attemptNumber);
        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_DICTATED_MESSAGE, dictatedMessage);
        context.startService(serviceIntent);
    }

    protected void readNext(Context context) {
        Intent serviceIntent = new Intent(context, SayMyTextService.class);
        serviceIntent.setAction(SayMyTextService.ACTION_READ_NEXT_SMS);
        context.startService(serviceIntent);
    }
}
