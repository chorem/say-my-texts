/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.broadcastreceiver;

import android.content.Intent;

/*
 * #%L
 * Say My Texts
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.util.Log;
import org.chorem.android.saymytexts.SayMyTextService;
import org.chorem.android.saymytexts.SayMyTextsApplication;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.2
 */
public class DeviceConnectionBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "DeviceConnectionBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, SayMyTextService.class);
        String action = intent.getAction();

        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "onReceive " + action);
        }

        if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
            int headSetState = intent.getIntExtra("state", 0);
            serviceIntent.setAction(SayMyTextService.ACTION_HEADSET_PLUGGED);
            serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_HEADSET_PLUGGED, headSetState > 0);
            context.startService(serviceIntent);

        } else {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            serviceIntent.setAction(SayMyTextService.ACTION_MANAGE_BT_DEVICE);

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                BluetoothClass bluetoothClass = device.getBluetoothClass();
                if (bluetoothClass != null) {
                    int majorDeviceClass = bluetoothClass.getMajorDeviceClass();
                    if (majorDeviceClass == BluetoothClass.Device.Major.AUDIO_VIDEO) {
                        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_BT_DEVICE, device);
                        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_ADD_BT_DEVICE, true);
                        context.startService(serviceIntent);
                    }

                } else {
                    //error recognizing the bt class
                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.e(TAG, "error recognizing the BT class of the connected device");
                    }
                }

            } else {
                serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_BT_DEVICE, device);
                serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_ADD_BT_DEVICE, false);
                context.startService(serviceIntent);
            }
        }
    }
}
