/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import org.chorem.android.saymytexts.SayMyTextService;
import org.chorem.android.saymytexts.SayMyTextsApplication;
import org.chorem.android.saymytexts.model.SMS;
import org.chorem.android.saymytexts.utils.SayMyTextsRecognitionListener;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public class DictateSmsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "DictateSmsBroadcastReceiver";

    public static final String ACTION_DICTATE_SMS = "org.chorem.android.saymytexts.DICTATE_SMS";

    public static final String INTENT_EXTRA_SMS = "sms";
    public static final String INTENT_EXTRA_ATTEMPT_NUMBER = "attemptNumber";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "next action ?");
        }
        final SMS sms = (SMS) intent.getSerializableExtra(INTENT_EXTRA_SMS);

        if (sms != null) {
            SpeechRecognizer speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
            speechRecognizer.setRecognitionListener(new SayMyTextsRecognitionListener(context, intent, sms) {

                @Override
                protected void onError(Context context, Intent intent, SMS sms, int error) {
                    reaskToDictate(context, intent, sms);
                }

                @Override
                public void onResults(Bundle data) {
                    List<String> results = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.d(TAG, "results " + results);
                    }
                    if (results != null) {
                        String text = results.get(0);
                        Intent serviceIntent = new Intent(context, SayMyTextService.class);
                        serviceIntent.setAction(SayMyTextService.ACTION_CONFIRM_SMS_SENDING);
                        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_SMS, sms);
                        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_DICTATED_MESSAGE, text);
                        context.startService(serviceIntent);

                    } else {
                        tg.startTone(ToneGenerator.TONE_PROP_NACK);
                        reaskToDictate(context, intent, sms);
                    }
                }

            });

            Intent recognizeIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            // Specify free form input
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                                     RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

            speechRecognizer.startListening(recognizeIntent);
        }
    }

    private void reaskToDictate(Context context, Intent intent, SMS sms) {
        int attemptNumber = intent.getIntExtra(INTENT_EXTRA_ATTEMPT_NUMBER, 1);

        Intent serviceIntent = new Intent(context, SayMyTextService.class);
        serviceIntent.setAction(SayMyTextService.ACTION_DICTATE_SMS);
        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_SMS, sms);
        serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_ATTEMPT_NUMBER, attemptNumber);
        context.startService(serviceIntent);
    }
}
