/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.speech.tts.TextToSpeech;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import org.chorem.android.saymytexts.utils.SayMyTextsUtils;

/**
 * Activity to set settings
 *
 * @author Kevin Morin (Code Lutin)
 * @since 1.0
 */
public class SettingsActivity extends Activity {

    private static final String TAG = "SettingsActivity";

    private static final String ABOUT_DIALOG_TAG = "about";

    private static final int CHECK_TTS_REQUEST_CODE = 42;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

        try {
            Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkIntent, CHECK_TTS_REQUEST_CODE);

        } catch(ActivityNotFoundException eee) {
            if (SayMyTextsApplication.LOG_ENABLED) {
                Log.e(TAG, "No activity found fo texttospeech", eee);
            }
            Toast.makeText(this, R.string.texttospeech_not_installed, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHECK_TTS_REQUEST_CODE) {

            try {
                if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL) {
                    Intent installIntent = new Intent(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    startActivity(installIntent);
                }
                Intent serviceIntent = new Intent(this, SayMyTextService.class);
                this.startService(serviceIntent);

            } catch(ActivityNotFoundException eee) {
                if (SayMyTextsApplication.LOG_ENABLED) {
                    Log.e(TAG, "No activity found fo texttospeech", eee);
                }
                Toast.makeText(this, R.string.texttospeech_not_installed, Toast.LENGTH_LONG).show();
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void openAbout(MenuItem item) {
        DialogFragment newFragment = new AboutDialogFragment();
        newFragment.show(getFragmentManager(), ABOUT_DIALOG_TAG);
    }

    public static class SettingsFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            Preference testPreference = findPreference(getString(R.string.preference_test_sms_key));
            testPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    sendSMS();
                    return true;
                }
            });

            Context context = getActivity();

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

            String key = getString(R.string.preference_reading_profile_key);
            ListPreference listPreference = (ListPreference) findPreference(key);
            CharSequence summary = listPreference.getEntry();
            listPreference.setSummary(summary);

            key = getString(R.string.preference_reading_caller_profile_key);
            listPreference = (ListPreference) findPreference(key);
            summary = listPreference.getEntry();
            listPreference.setSummary(summary);

            key = getString(R.string.preference_enable_interaction_key);
            SwitchPreference switchPreference = (SwitchPreference) findPreference(key);
            boolean enabled = SayMyTextsUtils.checkVoiceRecognition(context);
            switchPreference.setEnabled(enabled);
            boolean interactionEnabled = enabled && sharedPref.getBoolean(key, switchPreference.isChecked());
            switchPreference.setChecked(interactionEnabled);

            key = getString(R.string.preference_voice_recognizer_max_attempt_number_key);
            String maxAttemptValue = sharedPref.getString(key, null);
            int maxAttemptNumber;
            try {
                maxAttemptNumber = Integer.parseInt(maxAttemptValue);
            } catch (NumberFormatException e) {
                maxAttemptNumber = 3;
            }
            Preference preference = findPreference(key);
            preference.setSummary(String.valueOf(maxAttemptNumber));
            preference.setEnabled(interactionEnabled);

            key = getString(R.string.preference_enable_heisendroid_mode_key);
            switchPreference = (SwitchPreference) findPreference(key);
            enabled = sharedPref.getBoolean(key, switchPreference.isChecked());
            switchPreference.setChecked(enabled);

            key = getString(R.string.preference_notifications_key);
            listPreference = (ListPreference) findPreference(key);
            summary = listPreference.getEntry();
            listPreference.setSummary(summary);

            sharedPref.registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
            sharedPref.unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            if (getString(R.string.preference_reading_profile_key).equals(key)) {
                ListPreference preference = (ListPreference) findPreference(key);
                CharSequence summary = preference.getEntry();
                preference.setSummary(summary);

            } else if (getString(R.string.preference_reading_caller_profile_key).equals(key)) {
                ListPreference preference = (ListPreference) findPreference(key);
                CharSequence summary = preference.getEntry();
                preference.setSummary(summary);

            } else if (getString(R.string.preference_enable_interaction_key).equals(key)) {
                SwitchPreference switchPreference = (SwitchPreference) findPreference(key);
                boolean enabled = sharedPreferences.getBoolean(key, switchPreference.isChecked());
                switchPreference.setChecked(enabled);
                findPreference(getString(R.string.preference_voice_recognizer_max_attempt_number_key)).setEnabled(enabled);

            } else if (getString(R.string.preference_voice_recognizer_max_attempt_number_key).equals(key)) {
                String maxAttemptValue = sharedPreferences.getString(key, null);
                int maxAttemptNumber;
                try {
                    maxAttemptNumber = Integer.parseInt(maxAttemptValue);
                } catch (NumberFormatException e) {
                    maxAttemptNumber = 3;
                }
                findPreference(key).setSummary(String.valueOf(maxAttemptNumber));

            } else if (getString(R.string.preference_enable_heisendroid_mode_key).equals(key)) {
                SwitchPreference switchPreference = (SwitchPreference) findPreference(key);
                boolean enabled = sharedPreferences.getBoolean(key, switchPreference.isChecked());
                switchPreference.setChecked(enabled);

            } else if (getString(R.string.preference_notifications_key).equals(key)) {
                ListPreference preference = (ListPreference) findPreference(key);
                CharSequence summary = preference.getEntry();
                preference.setSummary(summary);
            }
        }

        private void sendSMS() {
            Context context = getActivity();
            TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String phoneNumber = tMgr.getLine1Number();
            sendSMS(phoneNumber);
        }

        private void sendSMS(String phoneNumber) {
            Context context = getActivity();
            String message = getString(R.string.test_sms_content);

            PendingIntent pi = PendingIntent.getActivity(context, -1, new Intent(context, SettingsActivity.class), 0);
            SmsManager sms = SmsManager.getDefault();

            try {
                sms.sendTextMessage(phoneNumber, null, message, pi, null);

            } catch (Exception e) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle(R.string.preference_ask_phone_number_title);
                alert.setMessage(R.string.preference_ask_phone_number_message);

                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_PHONE);
                alert.setView(input);

                alert.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        sendSMS(value);
                    }
                });

                alert.setNegativeButton(android.R.string.cancel,  null);

                alert.show();
            }
        }
    }
}
