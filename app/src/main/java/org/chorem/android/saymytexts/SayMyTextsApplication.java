/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts;

import android.app.Application;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 1.0
 */
@ReportsCrashes(formKey = "", // will not be used
               mailTo = "Say-my-texts-devel@list.chorem.org",
               mode = ReportingInteractionMode.DIALOG,
               resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
               resDialogText = R.string.crash_dialog_text,
               resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
               resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
               resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
               resDialogOkToast = R.string.crash_dialog_ok_toast) // optional. displays a Toast message when the user accepts to send a report.

public class SayMyTextsApplication extends Application {

    public static final boolean LOG_ENABLED = true;

    @Override
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
    }

}
