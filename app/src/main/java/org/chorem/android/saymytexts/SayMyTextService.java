/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import org.chorem.android.saymytexts.broadcastreceiver.DictateSmsBroadcastReceiver;
import org.chorem.android.saymytexts.broadcastreceiver.SayNextActionBroadcastReceiver;
import org.chorem.android.saymytexts.model.Configuration;
import org.chorem.android.saymytexts.model.SMS;
import org.chorem.android.saymytexts.utils.SayMyTextsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Service which say out loud the text passed in the intent
 *
 * @author Kevin Morin (Code Lutin)
 * @since 1.0
 */
public class SayMyTextService extends Service implements TextToSpeech.OnInitListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "SayMyTextService";

    public static final String ACTION_READ_SMS = "org.chorem.android.saymytexts.READ_SMS";
    public static final String ACTION_READ_NEXT_SMS = "org.chorem.android.saymytexts.READ_NEXT_SMS";
    public static final String ACTION_MANAGE_BT_DEVICE = "org.chorem.android.saymytexts.ADD_BT_DEVICE";
    private static final String ACTION_REASK_ACTION = "org.chorem.android.saymytexts.REASK_ACTION";
    public static final String ACTION_DICTATE_SMS = "org.chorem.android.saymytexts.DICTATE_SMS";
    public static final String ACTION_CONFIRM_SMS_SENDING = "org.chorem.android.saymytexts.CONFIRM_SMS_SENDING";
    public static final String ACTION_HEADSET_PLUGGED = "org.chorem.android.saymytexts.HEADSET_PLUGGED";

    /** SMS to read */
    public static final String INTENT_EXTRA_SMS = "sms";
    /** Bluetooth device which has just connected or disconnected */
    public static final String INTENT_EXTRA_BT_DEVICE = "btDevice";
    /** If true, the device has just connected, else disconnected */
    public static final String INTENT_EXTRA_ADD_BT_DEVICE = "addBtDevice";
    /** Bluetooth device which has just connected or disconnected */
    public static final String INTENT_EXTRA_DICTATED_MESSAGE = "dictatedMessage";
    /** Attempt number: if set, it means that the user said something not understandable, so ask again */
    public static final String INTENT_EXTRA_ATTEMPT_NUMBER = "attemptNumber";
    /** If true, a headset is plugged, false otherwise */
    public static final String INTENT_EXTRA_HEADSET_PLUGGED = "headsetPlugged";

    /** utterance id when the bluetooth device is connected */
    private static final String BT_ASK_NEXT_ACTION_UTTERANCE_ID = "btAskNextActionUtteranceId";
    private static final String ASK_NEXT_ACTION_UTTERANCE_ID = "askNextActionUtteranceId";
    private static final String OTHER_UTTERANCE_ID = "otherUtteranceId";

    private static final int NOTIFICATION_ID = 42;

    private Configuration configuration = new Configuration();

    private AudioManager audioManager;

    /** null if the texttospeech is not initialized */
    private Boolean canSpeak = null;

    // true if the speaker was on when the first message in queue arrived
    private boolean speakerWasOn;

    private TextToSpeech textToSpeech;

    /** texts to read, received before the textospeech is ready or while a call is in progress */
    private List<SMS> awaitingTexts = new ArrayList<>();

    /** bluetooth devices which are currently connected */
    private Map<BluetoothDevice, Integer> bluetoothDevices = new HashMap<>();

    private boolean headsetPlugged = false;

    /**
     * Listener to call state change
     */
    private final PhoneStateListener callStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    final String contactName = SayMyTextsUtils.getContactDisplayNameByNumber(getApplicationContext(), incomingNumber);

                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.d(TAG, "should say " + contactName);
                    }

                    if (configuration.isReadingCallerActive()) {
                        String ringtone = getString(R.string.call_received, contactName);

                        textToSpeech.setLanguage(Locale.getDefault());
                        textToSpeech.setSpeechRate(1f);
                        textToSpeech.setPitch(1f);

                        HashMap<String, String> params = new HashMap<>();
                        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, OTHER_UTTERANCE_ID);
                        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_RING));

                        textToSpeech.speak(ringtone, TextToSpeech.QUEUE_ADD, params);
                    }
                    break;
            }

            super.onCallStateChanged(state, incomingNumber);

            if (canSpeak != null) {
                setCanSpeak(state == TelephonyManager.CALL_STATE_IDLE);
            }
        }

    };

    @Override
    public void onCreate() {
        super.onCreate();

        textToSpeech = new TextToSpeech(this, this);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        tm.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        IntentFilter intentFilter = new IntentFilter(SayNextActionBroadcastReceiver.ACTION_SAY_NEXT_ACTION);
        registerReceiver(new SayNextActionBroadcastReceiver(), intentFilter);

        intentFilter = new IntentFilter(DictateSmsBroadcastReceiver.ACTION_DICTATE_SMS);
        registerReceiver(new DictateSmsBroadcastReceiver(), intentFilter);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String key = getString(R.string.preference_voice_recognizer_max_attempt_number_key);
        String maxAttemptValue = sharedPref.getString(key, null);
        int maxAttemptNumber;
        try {
            maxAttemptNumber = Integer.parseInt(maxAttemptValue);
        } catch (NumberFormatException e) {
            maxAttemptNumber = 3;
        }
        configuration.setMaxAttemptNumber(maxAttemptNumber);

        String[] readingProfileValues = getResources().getStringArray(R.array.preferences_reading_profile_values);
        String readingProfileKey = getString(R.string.preference_reading_profile_key);
        configuration.setReadingProfile(sharedPref.getString(readingProfileKey, readingProfileValues[0]));

        String readingCallerProfileKey = getString(R.string.preference_reading_caller_profile_key);
        configuration.setReadingCallerProfile(sharedPref.getString(readingCallerProfileKey, readingProfileValues[0]));

        String heisendroidModeEnabledKey = getString(R.string.preference_enable_heisendroid_mode_key);
        configuration.setHeisendroidModeEnabled(sharedPref.getBoolean(heisendroidModeEnabledKey, true));

        String interactionPrefKey = getString(R.string.preference_enable_interaction_key);
        configuration.setInteractionEnabled(sharedPref.getBoolean(interactionPrefKey, true));

        String[] notificationsValues = getResources().getStringArray(R.array.preferences_notifications_values);
        String notificationMode = sharedPref.getString(key, notificationsValues[1]);
        configuration.setNotificationsEnabled(!notificationMode.equals(notificationsValues[0]));
        configuration.setNotificationsCancelable(!notificationMode.equals(notificationsValues[2]));

        updateReadingEnabled();

        sharedPref.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        tm.listen(callStateListener, PhoneStateListener.LISTEN_NONE);
        textToSpeech.shutdown();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);

        if (!SayMyTextsUtils.checkVoiceRecognition(this)) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPref.edit().putBoolean(getString(R.string.preference_enable_interaction_key), false).apply();
        }

        if (intent != null) {
            String action = intent.getAction();
            if (SayMyTextsApplication.LOG_ENABLED) {
                Log.d(TAG, "action " + action);
            }

            if (action != null) {
                final SMS sms = (SMS) intent.getSerializableExtra(INTENT_EXTRA_SMS);
                int attemptNumber = intent.getIntExtra(INTENT_EXTRA_ATTEMPT_NUMBER, 0);

                switch (action) {
                    case ACTION_MANAGE_BT_DEVICE:
                        BluetoothDevice device = intent.getParcelableExtra(INTENT_EXTRA_BT_DEVICE);
                        // if a device is passed to the service
                        // add or remove the device from the connected devices
                        if (device != null) {
                            boolean addBtDevice = intent.getBooleanExtra(INTENT_EXTRA_ADD_BT_DEVICE, false);
                            if (addBtDevice) {
                                bluetoothDevices.put(device, device.getBluetoothClass().getDeviceClass());
                            } else {
                                bluetoothDevices.remove(device);
                            }
                            updateReadingEnabled();
                        }
                        break;

                    case ACTION_HEADSET_PLUGGED:
                        headsetPlugged = intent.getBooleanExtra(INTENT_EXTRA_HEADSET_PLUGGED, false);
                        updateReadingEnabled();
                        break;

                    case ACTION_REASK_ACTION:
                        askForActionAfterReading(sms, !bluetoothDevices.isEmpty(), attemptNumber + 1);
                        break;

                    case ACTION_DICTATE_SMS:
                        dictateSMS(sms, attemptNumber + 1);
                        break;

                    case ACTION_CONFIRM_SMS_SENDING:
                        // if a message has just been dictated
                        final String dictatedMessage = intent.getStringExtra(INTENT_EXTRA_DICTATED_MESSAGE);
                        askSendingConfirmation(dictatedMessage, sms, attemptNumber + 1);
                        break;

                    case ACTION_READ_NEXT_SMS:
                        setCanSpeak(true);
                        break;

                    default:
                        if (configuration.isReadingActive()) {
                            if (!Boolean.FALSE.equals(canSpeak)) {
                                speakerWasOn = audioManager.isSpeakerphoneOn();

                                // if read but no external device is connected, play through the speaker
                                String[] readingProfileValues = getResources().getStringArray(R.array.preferences_reading_profile_values);
                                if (readingProfileValues[0].equals(configuration.getReadingProfile())
                                        && !headsetPlugged && bluetoothDevices.isEmpty()) {
                                    audioManager.setMode(AudioManager.MODE_IN_CALL);
                                    audioManager.setSpeakerphoneOn(true);
                                }
                            }

                            if (Boolean.TRUE.equals(canSpeak)) {
                                requestReading(sms);

                            } else {
                                awaitingTexts.add(sms);
                            }
                        }
                }
            }

            result = START_STICKY;
        }

        return result;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            setCanSpeak(tm.getCallState() == TelephonyManager.CALL_STATE_IDLE);

        } else {
            setCanSpeak(null);
            Toast.makeText(this, R.string.texttospeech_init_error, Toast.LENGTH_LONG).show();
            this.stopSelf();
        }
    }

    /**
     * Sets if the texts can be read
     *
     * @param canSpeak null if the texttospeech is not ready,
     *                 false if a call is in progress
     *                 true otherwise
     */
    private void setCanSpeak(Boolean canSpeak) {
        this.canSpeak = canSpeak;

        if (Boolean.TRUE.equals(canSpeak)) {
            if (!awaitingTexts.isEmpty()) {
                SMS sms = awaitingTexts.remove(0);
                requestReading(sms);

            } else {
                if (!bluetoothDevices.isEmpty()) {
                    audioManager.stopBluetoothSco();
                }
                audioManager.setSpeakerphoneOn(speakerWasOn);
                audioManager.setMode(AudioManager.MODE_NORMAL);
                audioManager.abandonAudioFocus(null);
            }
        } else {
            audioManager.requestAudioFocus(null,
                                            // Use the music stream.
                                            AudioManager.STREAM_VOICE_CALL,
                                            // Request permanent focus.
                                            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }
    }

    /**
     * Requests the reading of one text through the wired headset or bluetooth device
     * @param sms the text to read
     */
    private void requestReading(final SMS sms) {
        if (bluetoothDevices.isEmpty()) {
            readText(sms, false);
        } else {
            requestBluetoothSpeakingActivation(new Function() {
                @Override
                public void apply() {
                    readText(sms, true);
                }
            });
        }
    }

    /**
     * Starts the connection with the bluetooth device and requests the reading
     * @param callback the function called when the bluetooth is ready
     */
    private void requestBluetoothSpeakingActivation(final Function callback) {
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int state = intent.getExtras().getInt(AudioManager.EXTRA_SCO_AUDIO_STATE);
                if (state == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
                    context.unregisterReceiver(this);
                    callback.apply();
                }
            }
        }, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED));
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        audioManager.startBluetoothSco();
    }

    /**
     * Reads the texts out loud
     * @param sms the text to read
     * @param btConnected if true, adds the utterance id for the bluetooth device
     */
    private void readText(final SMS sms, boolean btConnected) {
        // disable the reading of the nexts sms while reading the current one
        setCanSpeak(false);

        String message = sms.getMessage();
        if (message.trim().toLowerCase().equals(getString(R.string.konami_code))) {

            SayMyTextsUtils.playFSSong();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(getString(R.string.fs_song_url)));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            setCanSpeak(true);

        } else {
            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onStart(String utteranceId) {
                }

                @Override
                public void onError(String utteranceId) {
                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.e(TAG, "Error speaking: " + utteranceId);
                    }
                }

                @Override
                public void onDone(String utteranceId) {
                    if (SayMyTextsApplication.LOG_ENABLED) {
                        Log.d(TAG, "done");
                    }
                    setCanSpeak(true);
                }
            });

            HashMap<String, String> params = new HashMap<>();
            params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_VOICE_CALL));

            boolean heisendroidModeEnabled = configuration.isHeisendroidModeEnabled();
            boolean interactionEnabled = configuration.isInteractionEnabled();

            if (heisendroidModeEnabled) {
                textToSpeech.setLanguage(Locale.US);
                textToSpeech.setSpeechRate(0.3f);
                textToSpeech.setPitch(0.1f);
                textToSpeech.speak("Say my text.", TextToSpeech.QUEUE_ADD, params);
            }

            if (!interactionEnabled && !heisendroidModeEnabled) {
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, OTHER_UTTERANCE_ID);
            }
            textToSpeech.setLanguage(Locale.getDefault());
            textToSpeech.setSpeechRate(1f);
            textToSpeech.setPitch(1f);
            String text = getString(R.string.sms_received, sms.getSenderName(), message);
            textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, params);

            if (heisendroidModeEnabled) {
                if (interactionEnabled) {
                    params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, OTHER_UTTERANCE_ID);
                }
                textToSpeech.setLanguage(Locale.US);
                textToSpeech.setSpeechRate(0.3f);
                textToSpeech.setPitch(0.1f);
                textToSpeech.speak("You're goddamn right.", TextToSpeech.QUEUE_ADD, params);
            }

            if (interactionEnabled) {
                askForActionAfterReading(sms, btConnected, 1);
            }
        }
    }

    private void askForActionAfterReading(final SMS sms, boolean btConnected, final int attemptNumber) {
        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
            }

            @Override
            public void onError(String utteranceId) {
                if (SayMyTextsApplication.LOG_ENABLED) {
                    Log.e(TAG, "Error speaking: " + utteranceId);
                }
            }

            @Override
            public void onDone(String utteranceId) {
                if (ASK_NEXT_ACTION_UTTERANCE_ID.equals(utteranceId) ||
                        BT_ASK_NEXT_ACTION_UTTERANCE_ID.equals(utteranceId)) {
                    Intent sayAction = new Intent(SayNextActionBroadcastReceiver.ACTION_SAY_NEXT_ACTION);
                    sayAction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_SMS, sms);
                    sayAction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_ATTEMPT_NUMBER, attemptNumber);
                    sayAction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_FALLBACK_ACTION, ACTION_REASK_ACTION);

                    sendBroadcast(sayAction);
                }
            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_VOICE_CALL));

        textToSpeech.setLanguage(Locale.getDefault());
        textToSpeech.setSpeechRate(1f);
        textToSpeech.setPitch(1f);

        if (attemptNumber > 1) {
            textToSpeech.speak(getString(R.string.voice_not_recognized), TextToSpeech.QUEUE_ADD, params);
        }

        if (attemptNumber <= configuration.getMaxAttemptNumber()) {
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID,
                       btConnected ? BT_ASK_NEXT_ACTION_UTTERANCE_ID : ASK_NEXT_ACTION_UTTERANCE_ID);
            textToSpeech.speak(getString(R.string.ask_next_action), TextToSpeech.QUEUE_ADD, params);

        } else {
            setCanSpeak(true);
        }
    }

    private void dictateSMS(final SMS sms, final int attemptNumber) {
        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "dictateSMS ");
        }
        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
            }

            @Override
            public void onError(String utteranceId) {
                if (SayMyTextsApplication.LOG_ENABLED) {
                    Log.e(TAG, "Error speaking: " + utteranceId);
                }
            }

            @Override
            public void onDone(String utteranceId) {
                Intent dictateAction = new Intent(DictateSmsBroadcastReceiver.ACTION_DICTATE_SMS);
                dictateAction.putExtra(DictateSmsBroadcastReceiver.INTENT_EXTRA_SMS, sms);
                dictateAction.putExtra(DictateSmsBroadcastReceiver.INTENT_EXTRA_ATTEMPT_NUMBER, attemptNumber);
                sendBroadcast(dictateAction);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_VOICE_CALL));

        if (attemptNumber > 1) {
            textToSpeech.speak(getString(R.string.voice_not_recognized), TextToSpeech.QUEUE_ADD, params);
        }

        if (attemptNumber <= configuration.getMaxAttemptNumber()) {
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, OTHER_UTTERANCE_ID);
            textToSpeech.setLanguage(Locale.getDefault());
            textToSpeech.setSpeechRate(1f);
            textToSpeech.setPitch(1f);
            textToSpeech.speak(getString(R.string.dictate_sms), TextToSpeech.QUEUE_ADD, params);

        } else {
            setCanSpeak(true);
        }

    }

    private void askSendingConfirmation(final String message, final SMS originSms, final int attemptNumber) {
        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "askSendingConfirmation " + message);
        }
        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
            }

            @Override
            public void onError(String utteranceId) {
                if (SayMyTextsApplication.LOG_ENABLED) {
                    Log.e(TAG, "Error speaking: " + utteranceId);
                }
            }

            @Override
            public void onDone(String utteranceId) {
                Intent sayaction = new Intent(SayNextActionBroadcastReceiver.ACTION_SAY_NEXT_ACTION);
                sayaction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_SMS, originSms);
                sayaction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_MESSAGE, message);
                sayaction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_ATTEMPT_NUMBER, attemptNumber);
                sayaction.putExtra(SayNextActionBroadcastReceiver.INTENT_EXTRA_FALLBACK_ACTION, ACTION_CONFIRM_SMS_SENDING);
                sendBroadcast(sayaction);

            }
        });

        HashMap<String, String> params = new HashMap<>();
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_VOICE_CALL));

        textToSpeech.setLanguage(Locale.getDefault());
        textToSpeech.setSpeechRate(1f);
        textToSpeech.setPitch(1f);

        if (attemptNumber > 1) {
            textToSpeech.speak(getString(R.string.voice_not_recognized), TextToSpeech.QUEUE_ADD, params);
        }

        if (attemptNumber <= configuration.getMaxAttemptNumber()) {
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, OTHER_UTTERANCE_ID);
            String text = getString(R.string.send_sms_confirmation, message);
            textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, params);

        } else {
            setCanSpeak(true);
        }

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.preference_voice_recognizer_max_attempt_number_key))) {
            String maxAttemptValue = sharedPreferences.getString(key, null);
            int maxAttemptNumber;
            try {
                maxAttemptNumber = Integer.parseInt(maxAttemptValue);
            } catch (NumberFormatException e) {
                maxAttemptNumber = 3;
            }
            configuration.setMaxAttemptNumber(maxAttemptNumber);

        } else if (key.equals(getString(R.string.preference_reading_profile_key))) {
            String[] readingProfileValues = getResources().getStringArray(R.array.preferences_reading_profile_values);
            configuration.setReadingProfile(sharedPreferences.getString(key, readingProfileValues[0]));
            updateReadingEnabled();

        } else if (key.equals(getString(R.string.preference_reading_caller_profile_key))) {
            String[] readingProfileValues = getResources().getStringArray(R.array.preferences_reading_profile_values);
            configuration.setReadingCallerProfile(sharedPreferences.getString(key, readingProfileValues[0]));
            updateReadingEnabled();

        } else if (key.equals(getString(R.string.preference_enable_heisendroid_mode_key))) {
            configuration.setHeisendroidModeEnabled(sharedPreferences.getBoolean(key, true));

        } else if (key.equals(getString(R.string.preference_enable_interaction_key))) {
            configuration.setInteractionEnabled(sharedPreferences.getBoolean(key, true));

        } else if (key.equals(getString(R.string.preference_notifications_key))) {
            String[] notificationsValues = getResources().getStringArray(R.array.preferences_notifications_values);
            String notificationMode = sharedPreferences.getString(key, notificationsValues[1]);
            configuration.setNotificationsEnabled(!notificationMode.equals(notificationsValues[0]));
            configuration.setNotificationsCancelable(!notificationMode.equals(notificationsValues[2]));
            updateReadingEnabled();
        }
    }

    private void updateReadingEnabled() {
        String[] readingProfileValues = getResources().getStringArray(R.array.preferences_reading_profile_values);
        String[] readingProfileEntries = getResources().getStringArray(R.array.preferences_reading_profile_entries);
        String desc = null;

        // always read
        String readingProfile = configuration.getReadingProfile();
        boolean readingActive;
        if (readingProfileValues[0].equals(readingProfile)) {
            readingActive = true;
            desc = readingProfileEntries[0];
        }
        // if a bt device or a headset is connected
        else if (readingProfileValues[1].equals(readingProfile)) {
            readingActive = headsetPlugged || !bluetoothDevices.isEmpty();

            if (!bluetoothDevices.isEmpty()) {
                desc = getString(R.string.notification_reading_active_bt_device_connected);

            } else if (headsetPlugged) {
                desc = getString(R.string.notification_reading_active_headset_connected);
            }
        }
        // if a headset is connected
        else if (readingProfileValues[2].equals(readingProfile)) {
            readingActive = headsetPlugged;
            if (headsetPlugged) {
                desc = getString(R.string.notification_reading_active_headset_connected);
            }
        }
        // never read
        else {
            readingActive = false;
            desc = readingProfileEntries[3];
        }
        configuration.setReadingActive(readingActive);

        // always read
        String readingCallerProfile = configuration.getReadingCallerProfile();
        boolean alwaysReadCaller = readingProfileValues[0].equals(readingCallerProfile);
        boolean readingCallerActive;
        if (alwaysReadCaller) {
            readingCallerActive = true;
        }
        // if a bt device or a headset is connected
        else if (readingProfileValues[1].equals(readingCallerProfile)) {
            readingCallerActive = headsetPlugged || !bluetoothDevices.isEmpty();
        }
        // if a headset is connected
        else if (readingProfileValues[2].equals(readingCallerProfile)) {
            readingCallerActive = headsetPlugged;
        }
        // never read
        else {
            readingCallerActive = false;
        }
        configuration.setReadingCallerActive(readingCallerActive);

        updateNotification(desc);
    }

    private void updateNotification(String content) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (configuration.isNotificationsEnabled() && configuration.isReadingActive()) {
            BitmapDrawable logo = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_launcher_heisendroid);
            Notification.Builder builder = new Notification.Builder(this)
                    .setLargeIcon(logo.getBitmap())
                    .setContentText(content);
            builder.setSmallIcon(R.drawable.ic_stat_heisendroid)
                    .setContentTitle(getString(R.string.notification_reading_active_title));

            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, settingsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            Notification notification = builder.getNotification();
            if (!configuration.isNotificationsCancelable()) {
                notification.flags = Notification.FLAG_NO_CLEAR;
            }

            notificationManager.notify(NOTIFICATION_ID, notification);

        } else {
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    public interface Function {
        void apply();
    }
}
