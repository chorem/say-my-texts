/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.model;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.13
 */
public class Configuration {

    private int maxAttemptNumber;

    /** is reading enabled according to the settings */
    private boolean readingActive = false;

    private boolean readingCallerActive = false;

    private String readingProfile;

    private String readingCallerProfile;

    private boolean heisendroidModeEnabled;

    private boolean interactionEnabled;

    private boolean notificationsEnabled;

    private boolean notificationsCancelable;

    public int getMaxAttemptNumber() {
        return maxAttemptNumber;
    }

    public void setMaxAttemptNumber(int maxAttemptNumber) {
        this.maxAttemptNumber = maxAttemptNumber;
    }

    public boolean isReadingActive() {
        return readingActive;
    }

    public void setReadingActive(boolean readingActive) {
        this.readingActive = readingActive;
    }

    public boolean isReadingCallerActive() {
        return readingCallerActive;
    }

    public void setReadingCallerActive(boolean readingCallerActive) {
        this.readingCallerActive = readingCallerActive;
    }

    public String getReadingProfile() {
        return readingProfile;
    }

    public void setReadingProfile(String readingProfile) {
        this.readingProfile = readingProfile;
    }

    public String getReadingCallerProfile() {
        return readingCallerProfile;
    }

    public void setReadingCallerProfile(String readingCallerProfile) {
        this.readingCallerProfile = readingCallerProfile;
    }

    public boolean isHeisendroidModeEnabled() {
        return heisendroidModeEnabled;
    }

    public void setHeisendroidModeEnabled(boolean heisendroidModeEnabled) {
        this.heisendroidModeEnabled = heisendroidModeEnabled;
    }

    public boolean isInteractionEnabled() {
        return interactionEnabled;
    }

    public void setInteractionEnabled(boolean interactionEnabled) {
        this.interactionEnabled = interactionEnabled;
    }

    public boolean isNotificationsEnabled() {
        return notificationsEnabled;
    }

    public void setNotificationsEnabled(boolean notificationsEnabled) {
        this.notificationsEnabled = notificationsEnabled;
    }

    public boolean isNotificationsCancelable() {
        return notificationsCancelable;
    }

    public void setNotificationsCancelable(boolean notificationsCancelable) {
        this.notificationsCancelable = notificationsCancelable;
    }
}
