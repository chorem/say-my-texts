/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import org.chorem.android.saymytexts.model.SMS;
import org.chorem.android.saymytexts.SayMyTextService;
import org.chorem.android.saymytexts.SayMyTextsApplication;
import org.chorem.android.saymytexts.utils.SayMyTextsUtils;

/**
 * Receives the SMSs and if the headset is plugged, start the service to say it out loud.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 1.0
 */
public class NewTextBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "NewTextBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, SayMyTextService.class);
        String action = intent.getAction();
        if ("android.provider.Telephony.SMS_RECEIVED".equals(action)) {

            // get the SMS message passed in
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                SmsMessage[] msgs = null;
                StringBuilder messageReceived = new StringBuilder();

                // retrieve the SMS message received
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    messageReceived.append(msgs[i].getDisplayMessageBody()).append(" ");
                }

                // Get the Sender Phone Number
                String senderPhoneNumber = msgs[0].getDisplayOriginatingAddress();
                String senderName = SayMyTextsUtils.getContactDisplayNameByNumber(context, senderPhoneNumber);
                SMS sms = new SMS(senderPhoneNumber, senderName, messageReceived.toString());

                if (SayMyTextsApplication.LOG_ENABLED) {
                    Log.d(TAG, messageReceived.toString());
                }
                // start the service to say it out loud
                serviceIntent.putExtra(SayMyTextService.INTENT_EXTRA_SMS, sms);
                serviceIntent.setAction(SayMyTextService.ACTION_READ_SMS);
                context.startService(serviceIntent);
            }
        }
    }
}
