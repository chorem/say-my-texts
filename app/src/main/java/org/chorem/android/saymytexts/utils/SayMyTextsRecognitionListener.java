/*
 * Copyright (C) 2014 - 2017 Code Lutin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.limitations under the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package org.chorem.android.saymytexts.utils;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.util.Log;
import org.chorem.android.saymytexts.SayMyTextsApplication;
import org.chorem.android.saymytexts.model.SMS;

/**
 * @author Kevin Morin (Code Lutin)
 * @since x.x
 */
public abstract class SayMyTextsRecognitionListener implements RecognitionListener {

    private static final String TAG = "SayMyTextsRecognitionListener";

    protected Context context;
    protected Intent intent;
    protected SMS sms;

    protected ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, ToneGenerator.MAX_VOLUME);
    private boolean errorFired = false;

    public SayMyTextsRecognitionListener(Context context, Intent intent, SMS sms) {
        this.context = context;
        this.intent = intent;
        this.sms = sms;
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    @Override
    public void onRmsChanged(float rmsdB) {
    }

    @Override
    public void onBufferReceived(byte[] buffer) {}

    @Override
    public void onEndOfSpeech() {}

    @Override
    public void onError(int error) {
        if (SayMyTextsApplication.LOG_ENABLED) {
            Log.d(TAG, "onError " + error + " : " + errorFired);
        }
        tg.startTone(ToneGenerator.TONE_PROP_NACK);
        if (!errorFired) {
            onError(context, intent, sms, error);
        }
        errorFired = true;
    }

    @Override
    public void onPartialResults(Bundle partialResults) {}

    @Override
    public void onEvent(int eventType, Bundle params) {}

    protected abstract void onError(Context context, Intent intent, SMS sms, int error);

}
